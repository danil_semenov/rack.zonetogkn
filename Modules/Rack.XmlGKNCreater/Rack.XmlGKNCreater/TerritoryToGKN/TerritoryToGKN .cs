﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace XmlApp.TerritoryToGNK
{
	[XmlRoot(ElementName = "Agent", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Agent
	{
		[XmlElement(ElementName = "Appointment", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string Appointment { get; set; }
		[XmlElement(ElementName = "FamilyName", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string FamilyName { get; set; }
		[XmlElement(ElementName = "FirstName", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "Patronymic", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string Patronymic { get; set; }
	}

	[XmlRoot(ElementName = "Organization", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Organization
	{
		[XmlElement(ElementName = "Name", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string Name { get; set; }
		[XmlElement(ElementName = "Agent", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Agent Agent { get; set; }
		[XmlElement(ElementName = "AddressOrganization", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string AddressOrganization { get; set; }
		[XmlElement(ElementName = "CodeOGRN", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string CodeOGRN { get; set; }
	}

	[XmlRoot(ElementName = "Client", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Client
	{
		[XmlElement(ElementName = "Organization", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Organization Organization { get; set; }
		[XmlAttribute(AttributeName = "Date")]
		public string Date { get; set; }
	}

	[XmlRoot(ElementName = "Clients", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Clients
	{
		[XmlElement(ElementName = "Client", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Client Client { get; set; }
	}

	[XmlRoot(ElementName = "CadastralEngineer", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class CadastralEngineer
	{
		[XmlElement(ElementName = "FamilyName", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string FamilyName { get; set; }
		[XmlElement(ElementName = "FirstName", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "Patronymic", Namespace = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1")]
		public string Patronymic { get; set; }
		[XmlElement(ElementName = "NCertificate", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string NCertificate { get; set; }
		[XmlElement(ElementName = "Telephone", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string Telephone { get; set; }
		[XmlElement(ElementName = "Address", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string Address { get; set; }
		[XmlElement(ElementName = "Email", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1")]
		public string Email { get; set; }
		[XmlElement(ElementName = "INN", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string INN { get; set; }
		[XmlElement(ElementName = "Organization", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Organization Organization { get; set; }
	}

	[XmlRoot(ElementName = "Contractor", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Contractor
	{
		[XmlElement(ElementName = "CadastralEngineer", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public CadastralEngineer CadastralEngineer { get; set; }
	}

	[XmlRoot(ElementName = "Title", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Title
	{
		[XmlElement(ElementName = "Clients", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Clients Clients { get; set; }
		[XmlElement(ElementName = "Contractor", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Contractor Contractor { get; set; }
	}

	[XmlRoot(ElementName = "Ordinate", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class Ordinate
	{
		[XmlAttribute(AttributeName = "X")]
		public string X { get; set; }
		[XmlAttribute(AttributeName = "Y")]
		public string Y { get; set; }
		[XmlAttribute(AttributeName = "NumGeopoint")]
		public string NumGeopoint { get; set; }
		[XmlAttribute(AttributeName = "DeltaGeopoint")]
		public string DeltaGeopoint { get; set; }
		[XmlAttribute(AttributeName = "GeopointOpred")]
		public string GeopointOpred { get; set; }
	}

	[XmlRoot(ElementName = "SpelementUnit", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class SpelementUnit
	{
		[XmlElement(ElementName = "Ordinate", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public Ordinate Ordinate { get; set; }
		[XmlAttribute(AttributeName = "TypeUnit")]
		public string TypeUnit { get; set; }
		[XmlAttribute(AttributeName = "SuNmb")]
		public string SuNmb { get; set; }
	}

	[XmlRoot(ElementName = "SpatialElement", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class SpatialElement
	{
		[XmlElement(ElementName = "SpelementUnit", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public List<SpelementUnit> SpelementUnit { get; set; }
	}

	[XmlRoot(ElementName = "Border", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class Border
	{
		[XmlAttribute(AttributeName = "Spatial")]
		public string Spatial { get; set; }
		[XmlAttribute(AttributeName = "Point1")]
		public string Point1 { get; set; }
		[XmlAttribute(AttributeName = "Point2")]
		public string Point2 { get; set; }
	}

	[XmlRoot(ElementName = "Borders", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class Borders
	{
		[XmlElement(ElementName = "Border", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public List<Border> Border { get; set; }
	}

	[XmlRoot(ElementName = "EntitySpatial", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class EntitySpatial
	{
		[XmlElement(ElementName = "SpatialElement", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public SpatialElement SpatialElement { get; set; }
		[XmlElement(ElementName = "Borders", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public Borders Borders { get; set; }
		[XmlAttribute(AttributeName = "EntSys")]
		public string EntSys { get; set; }
	}

	[XmlRoot(ElementName = "AreaMeter", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class AreaMeter
	{
		[XmlElement(ElementName = "Area", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string Area { get; set; }
		[XmlElement(ElementName = "Unit", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string Unit { get; set; }
		[XmlElement(ElementName = "Inaccuracy", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public string Inaccuracy { get; set; }
	}

	[XmlRoot(ElementName = "Area", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Area
	{
		[XmlElement(ElementName = "AreaMeter", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public AreaMeter AreaMeter { get; set; }
	}

	[XmlRoot(ElementName = "CoordSystem", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
	public class CoordSystem
	{
		[XmlAttribute(AttributeName = "Name")]
		public string Name { get; set; }
		[XmlAttribute(AttributeName = "CsId")]
		public string CsId { get; set; }
	}

	[XmlRoot(ElementName = "CoordSystems", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class CoordSystems
	{
		[XmlElement(ElementName = "CoordSystem", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1")]
		public CoordSystem CoordSystem { get; set; }
	}

	[XmlRoot(ElementName = "AppliedFile", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class AppliedFile
	{
		[XmlAttribute(AttributeName = "Kind")]
		public string Kind { get; set; }
		[XmlAttribute(AttributeName = "Name")]
		public string Name { get; set; }
	}

	[XmlRoot(ElementName = "Diagram", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class Diagram
	{
		[XmlElement(ElementName = "AppliedFile", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public AppliedFile AppliedFile { get; set; }
	}

	[XmlRoot(ElementName = "TerritoryToGKN", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
	public class TerritoryToGKN
	{
		[XmlElement(ElementName = "Title", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Title Title { get; set; }
		[XmlElement(ElementName = "EntitySpatial", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public EntitySpatial EntitySpatial { get; set; }
		[XmlElement(ElementName = "Area", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Area Area { get; set; }
		[XmlElement(ElementName = "CoordSystems", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public CoordSystems CoordSystems { get; set; }
		[XmlElement(ElementName = "Diagram", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/territory-to-gkn/1.0.4")]
		public Diagram Diagram { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
		[XmlAttribute(AttributeName = "tns", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Tns { get; set; }
		[XmlAttribute(AttributeName = "DocI5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string DocI5 { get; set; }
		[XmlAttribute(AttributeName = "CadEng4", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string CadEng4 { get; set; }
		[XmlAttribute(AttributeName = "Spa2", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Spa2 { get; set; }
		[XmlAttribute(AttributeName = "NameSoftware")]
		public string NameSoftware { get; set; }
		[XmlAttribute(AttributeName = "VersionSoftware")]
		public string VersionSoftware { get; set; }
		[XmlAttribute(AttributeName = "GUID")]
		public string GUID { get; set; }
	}
}
