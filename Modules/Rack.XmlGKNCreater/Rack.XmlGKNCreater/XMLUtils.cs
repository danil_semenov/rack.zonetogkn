﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using XmlApp.TerritoryToGNK;

namespace XmlApp
{
    public static class XMLUtils
    {
        public static void CreateXml<T>(
            this T fileInstance,
            string filePath) 
            where T: class
        {
            var serializer = new XmlSerializer(typeof(T));
            var writer = new StreamWriter(filePath);
            serializer.Serialize(writer, fileInstance);
            writer.Close();

        }
    }
}
