﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using ReactiveUI;
using XmlApp.ZoneToGKN;

namespace XmlApp
{
    /// <summary>
    /// Interaction logic for ZoneToGKNMainWindow.xaml
    /// </summary>
    public partial class ZoneToGKNMainWindow : ReactiveWindow<ZoneToGKNViewModel>
    {
        public ZoneToGKNMainWindow()
        {
            InitializeComponent();

            this.BindCommand(ViewModel, x => x.Save, x => x.Save);
            this.BindCommand(ViewModel, x => x.AddDocument, x => x.AddDocument);

            this.OneWayBind(ViewModel, x => x.Declarants, x => x.Declarant.ItemsSource);
            this.Bind(ViewModel, x => x.SelectedDeclarant, x => x.Declarant.SelectedItem);
            this.Bind(ViewModel, x => x.GUID, x => x.GUID.Text);

            this.Bind(ViewModel, x => x.Title.CodeDocument, x => x.CodeDocument.Text);
            this.Bind(ViewModel, x => x.Title.Name, x => x.Name.Text);
            this.Bind(ViewModel, x => x.Title.Number, x => x.Number.Text);
            this.Bind(ViewModel, x => x.Title.Date, x => x.Date.Text);
            this.Bind(ViewModel, x => x.Title.IssueOrgan, x => x.IssueOrgan.Text);
            this.Bind(ViewModel, x => x.Title.Desc, x => x.Desc.Text);
            //this.Bind(ZoneToGKNViewModel, x => x.Documents, x => x.Documents.ItemsSource);
            this.Bind(ViewModel, x => x.NewZones.Zone.CadastralDistrict, x => x.CadastralDistrict.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.CodeZone, x => x.CodeZone.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.CodeZoneDoc, x => x.CodeZoneDoc.Text);

            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.ContentRestrictions, x => x.ContentRestrictions.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.ProtectedObject, x => x.ProtectedObject.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.CodeDocument, x => x.TerritoryCodeDocument.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.Name, x => x.TerritoryName.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.Date, x => x.TerritoryDate.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.IssueOrgan, x => x.TerritoryIssueOrgan.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.AppliedFile.GUID, x => x.TerritoryAppliedFileGuid.Text);
            this.Bind(ViewModel, x => x.NewZones.Zone.SpecialZone.Territory.AppliedFile.Name, x => x.TerritoryAppliedFileName.Text);



            Documents.AutoGenerateColumns = false;
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Код документа",
                Binding = new Binding(nameof(Document.CodeDocument))
            });
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Наименование документа",
                Binding = new Binding(nameof(Document.Name))
            });
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Номер документа",
                Binding = new Binding(nameof(Document.Number))
            });
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Дата выдачи документа",
                Binding = new Binding(nameof(Document.Date))
            });
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Орган, выдавший документ. Автор документа",
                Binding = new Binding(nameof(Document.IssueOrgan))
            });
            Documents.Columns.Add(new DataGridTextColumn
            {
                Header = "Относительный путь к файлу с изображением",
                Binding = new Binding($"{nameof(AppliedFile)}.{nameof(AppliedFile.Name)}")
            });


            Location.AutoGenerateColumns = false;
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "ОКАТО",
                Binding = new Binding(nameof(Location2.OKATO))
            });
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "КЛАДР",
                Binding = new Binding(nameof(Location2.KLADR))
            });
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "ОКТМО",
                Binding = new Binding(nameof(Location2.OKTMO))
            });
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "Почтовый индекс",
                Binding = new Binding(nameof(Location2.PostalCode))
            });
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "Код региона",
                Binding = new Binding(nameof(Location2.Region))
            });
            Location.Columns.Add(new DataGridTextColumn
            {
                Header = "Район",
                Binding = new Binding("District.Name")
            });

        }
    }
}