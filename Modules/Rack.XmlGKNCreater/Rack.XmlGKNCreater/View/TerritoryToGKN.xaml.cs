﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ReactiveUI;
using XmlApp.ViewModel;

namespace XmlApp.View
{
    /// <summary>
    /// Interaction logic for TerritoryToGKN.xaml
    /// </summary>
    public partial class TerritoryToGKN : ReactiveWindow<TerritoryToGKNViewModel>
    {
        public TerritoryToGKN()
        {
            DataContext = ViewModel = new TerritoryToGKNViewModel();
            InitializeComponent();
            this.BindCommand(ViewModel, x => x.Save, x => x.Save);
            this.BindCommand(ViewModel, x => x.AddPoints, x => x.AddPoints);

            this.Bind(ViewModel, x => x.GUID, x => x.GUID.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Date, x => x.ClientDate.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Organization.Name, x => x.ClientName.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Organization.Agent.Appointment, x => x.ClientAppointment.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Organization.Agent.FamilyName, x => x.ClientFamilyName.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Organization.Agent.FirstName, x => x.ClientFirstName.Text);
            this.Bind(ViewModel, x => x.Title.Clients.Client.Organization.Agent.Patronymic, x => x.ClientPatronymic.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.FamilyName, x => x.ContractorFamilyName.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.FirstName, x => x.ContractorFirstName.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Patronymic, x => x.ContractorPatronymic.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.NCertificate, x => x.ContractorNCertificate.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Telephone, x => x.ContractorTelephone.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Address, x => x.ContractorAddress.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.INN, x => x.ContractorINN.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Organization.Name, x => x.ContractorName.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Organization.AddressOrganization, x => x.ContractorAddressOrganization.Text);
            this.Bind(ViewModel, x => x.Title.Contractor.CadastralEngineer.Organization.CodeOGRN, x => x.ContractorCodeOGRN.Text);
            this.Bind(ViewModel, x => x.EntitySpatial.EntSys, x => x.EntSys.Text);
            this.Bind(ViewModel, x => x.Area.AreaMeter.Area, x => x.Area.Text);
            this.Bind(ViewModel, x => x.Area.AreaMeter.Inaccuracy, x => x.Inaccuracy.Text);
            this.Bind(ViewModel, x => x.CoordSystems.CoordSystem.Name, x => x.CoordSystemName.Text);
            this.Bind(ViewModel, x => x.CoordSystems.CoordSystem.CsId, x => x.CsId.Text);
            this.Bind(ViewModel, x => x.Diagram.AppliedFile.Kind,x => x.AppliedFileKind.Text);
            this.Bind(ViewModel, x => x.Diagram.AppliedFile.Name, x => x.AppliedFileName.Text);

        }
    }
}
