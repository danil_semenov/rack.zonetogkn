﻿using System;
using System.IO;
using System.Reactive;
using System.Xml.Serialization;
using ClosedXML.Excel;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using XmlApp.TerritoryToGNK;
using Title = XmlApp.TerritoryToGNK.Title;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace XmlApp.ViewModel
{
    public class TerritoryToGKNViewModel : ReactiveObject
    {
        private const string RegistrySheetName = "Точки";
        
        public TerritoryToGKNViewModel()
        {
            GUID =   Guid.NewGuid().ToString();
            Title = new Title
            {
                Clients = new Clients
                {
                    Client = new Client
                    {
                        Organization = new Organization
                        {
                            Agent = new Agent()
                        }
                    }
                },
                Contractor = new Contractor
                {
                    CadastralEngineer = new CadastralEngineer {Organization = new Organization()}
                }
            };
            EntitySpatial = new EntitySpatial
            {
                SpatialElement = new SpatialElement {SpelementUnit = new List<SpelementUnit>()},
                Borders = new Borders {Border = new List<Border>()}
            };
            Area = new Area {AreaMeter = new AreaMeter {Unit = "055"}};
            CoordSystems = new CoordSystems
            {
                CoordSystem = new CoordSystem()
            };
            Diagram = new Diagram
            {
                AppliedFile = new AppliedFile()
            };
            Save = ReactiveCommand.Create(() =>
            {
                var territoryToGKN = CreateTerritoryToGKN();
                CreateXML(ref territoryToGKN);
            });

            AddPoints = ReactiveCommand.Create(() => { ReadPointsFromExcel(); });
        }

        private void ReadPointsFromExcel()
        {
            var sheet = GetExcelSheet(@"C:\Users\Semenov\Documents\Точки.xlsx");
            for (var i = 3; i < sheet.RowCount(); i++)
            {
                if (!sheet.Cell(i, 1).TryGetValue(out string SuNmb) || string.IsNullOrWhiteSpace(SuNmb))
                    return;
                if (!sheet.Cell(i, 2).TryGetValue(out string TypeUnit))
                    Console.WriteLine($"Параметр TypeUnit не найден");
                if (!sheet.Cell(i, 3).TryGetValue(out string GeopointOpred))
                    Console.WriteLine($"Параметр GeopointOpred не найден");
                if (!sheet.Cell(i, 4).TryGetValue(out string NumGeopoint))
                    Console.WriteLine($"Параметр NumGeopoint не найден");
                if (!sheet.Cell(i, 5).TryGetValue(out string DeltaGeopoint))
                    Console.WriteLine($"Параметр DeltaGeopoint не найден");
                if (!sheet.Cell(i, 6).TryGetValue(out string X))
                    Console.WriteLine($"Параметр X не найден");
                if (!sheet.Cell(i, 7).TryGetValue(out string Y))
                    Console.WriteLine($"Параметр Y не найден");

                EntitySpatial.SpatialElement.SpelementUnit.Add(new SpelementUnit
                {
                    SuNmb = SuNmb,
                    TypeUnit = TypeUnit,
                    Ordinate = new Ordinate
                    {
                        GeopointOpred = GeopointOpred,
                        NumGeopoint = NumGeopoint,
                        DeltaGeopoint = DeltaGeopoint,
                        X = X,
                        Y = Y
                    }
                });

                if (i == 3)
                    continue;

                if (!sheet.Cell(i - 1, 4).TryGetValue(out string LastNumGeopoint))
                    Console.WriteLine($"Параметр LastNumGeopoint не найден");

                EntitySpatial.Borders.Border.Add(new Border
                {
                    Point2 = NumGeopoint,
                    Point1 = LastNumGeopoint,
                    Spatial = "1"
                });
            }
        }

        private IXLWorksheet GetExcelSheet(string filePath)
        {
            MemoryStream memoryStream = null;
            XLWorkbook workbook;
            byte[] fileContent;
            FileStream fileStream = null;
            try
            {
                fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                fileContent = fileStream.ReadToEnd();
                memoryStream = new MemoryStream(fileContent);
                workbook = new XLWorkbook(memoryStream);
            }
            finally
            {
                memoryStream?.Dispose();
                fileStream?.Dispose();
            }

            if (!workbook.TryGetWorksheet(RegistrySheetName, out var sheet))
                Console.WriteLine($"Лист {RegistrySheetName} не найден");
            return sheet;
        }

        private TerritoryToGKN CreateTerritoryToGKN()
        {
            var territoryToGKN = new TerritoryToGKN();
            SetAtribute(ref territoryToGKN);
            territoryToGKN.GUID = GUID;
            territoryToGKN.Title = Title;
            territoryToGKN.EntitySpatial = EntitySpatial;
            territoryToGKN.Area = Area;
            territoryToGKN.CoordSystems = CoordSystems;
            territoryToGKN.Diagram = Diagram;
            return territoryToGKN;
        }

        private void SetAtribute(ref TerritoryToGKN territoryToGKN)
        {
            territoryToGKN.NameSoftware = "ПКЗО";
            territoryToGKN.VersionSoftware = "5.3.6";
            territoryToGKN.Tns = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1";
            territoryToGKN.DocI5 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1";
            territoryToGKN.CadEng4 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/cadastral-engineer/4.1.1";
            territoryToGKN.Spa2 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/entity-spatial/2.0.1";
        }

        private void CreateXML(ref TerritoryToGKN territoryToGKN)
        {
            var serializer =
                new XmlSerializer(typeof(TerritoryToGKN));
            var writer = new StreamWriter($"TerritoryToGKN_{territoryToGKN.GUID}.xml");


            serializer.Serialize(writer, territoryToGKN);
            writer.Close();
        }

        public ReactiveCommand<Unit, Unit> AddPoints { get; }
        public ReactiveCommand<Unit, Unit> Save { get; }
        [Reactive] public Title Title { get; set; }
        [Reactive] public string GUID { get; set; }
        [Reactive] public EntitySpatial EntitySpatial { get; set; }
        [Reactive] public Area Area { get; set; }
        [Reactive] public CoordSystems CoordSystems { get; set; }

        [Reactive] public Diagram Diagram { get; set; }
    }

    public static class FileStreamEx
    {
        public static byte[] ReadToEnd(this FileStream stream)
        {
            var bytesCount = stream.Length - stream.Position;
            var content = new byte[bytesCount];
            stream.Read(content, 0, Convert.ToInt32(bytesCount));
            return content;
        }
    }
}