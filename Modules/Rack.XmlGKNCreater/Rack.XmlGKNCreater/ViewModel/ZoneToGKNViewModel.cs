﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Xml.Serialization;
using Rack.Localization;
using Rack.Shared;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using XmlApp.ZoneToGKN;

namespace XmlApp
{
    public class ZoneToGKNViewModel : ReactiveViewModel
    {
        public ZoneToGKNViewModel(
            ILocalizationService localizationService,
            IScreen hostScreen)
            : base(localizationService, hostScreen)
        {
            GUID = Guid.NewGuid().ToString();
            Documents = new List<Document>();
            Title = new Title();
            NewZones = new NewZones
            {
                Zone = new Zone
                {
                    Locations = new Locations(),
                    SpecialZone = new SpecialZone
                    {
                        Territory = new Territory {AppliedFile = new AppliedFile {Kind = "02"}}
                    }
                }
            };
            LocationZone = new List<Location2> {new Location2() {District = new District() {Type = "р-н"}}};

            Declarants = new List<Declarant>
            {
                new Declarant
                {
                    Governance = new Governance
                    {
                        Name =
                            "Департамент недропользования и природных ресурсов Ханты-Мансийского автономного округа - Югры",
                        GovernanceCode = "007001001002",
                        Location = new Location
                        {
                            OKATO = "71131000000",
                            KLADR = "86000001000011600",
                            OKTMO = "71871000",
                            Region = "86",
                            City = new City {Type = "г", Name = "Ханты-Мансийск"},
                            Street = new Street {Type = "ул", Name = "Студенческая"},
                            Level1 = new Level1 {Type = "д", Value = "2"}
                        },
                        Email = "depprirod@admhmao.ru",
                        Telephone = "+7(3467)35-30-03"
                    }
                }
            };

            Save = ReactiveCommand.Create(() =>
            {
                var zoneToGKN = CreateZoneToGkn();
                CreateXML(ref zoneToGKN);
            });
            AddDocument = ReactiveCommand.Create(() =>
            {
                Documents.Add(new Document {AppliedFile = new AppliedFile {Kind = "01"}});
            });
        }

        private ZoneToGKN.ZoneToGKN CreateZoneToGkn()
        {
            var zoneToGKN = new ZoneToGKN.ZoneToGKN();
            SetAtribute(ref zoneToGKN);
            zoneToGKN.GUID = GUID;
            zoneToGKN.Title = Title;
            zoneToGKN.Declarant = SelectedDeclarant;
            zoneToGKN.Documents = new Documents { Document = Documents };
            zoneToGKN.NewZones = NewZones;
            zoneToGKN.NewZones.Zone.Locations.Location2 = LocationZone.First();
            return zoneToGKN;
        }
        private void SetAtribute(ref ZoneToGKN.ZoneToGKN zoneToGKN)
        {
            zoneToGKN.NameSoftware = "ПКЗО";
            zoneToGKN.VersionSoftware = "5.3.6";
            zoneToGKN.Zon5 = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8";
            zoneToGKN.Zone4 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2";
            zoneToGKN.Person5 = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1";
            zoneToGKN.Tns = "urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1";
            zoneToGKN.Gov5 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1";
            zoneToGKN.AdrInp6 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1";
            zoneToGKN.Org4 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/organization/4.0.1";
            zoneToGKN.Sen5 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/sender/5.0.1";
            zoneToGKN.DocI5 = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1";
        }

        private void CreateXML(ref ZoneToGKN.ZoneToGKN zoneToGKN)
        {
            var serializer =
                new XmlSerializer(typeof(ZoneToGKN.ZoneToGKN));
            var writer = new StreamWriter($"ZoneToGKN_{zoneToGKN.GUID}.xml");


            serializer.Serialize(writer, zoneToGKN);
            writer.Close();
        }


        [Reactive] public List<Declarant> Declarants { get; set; }
        [Reactive] public Declarant SelectedDeclarant { get; set; }
        [Reactive] public List<Document> Documents { get; set; }
        [Reactive] public Title Title { get; set; }
        [Reactive] public NewZones NewZones { get; set; }
        [Reactive] public List<Location2> LocationZone { get; set; }
        [Reactive] public string GUID { get; set; }
        public ReactiveCommand<Unit, Unit> Save { get; }
        public ReactiveCommand<Unit, Unit> AddDocument { get; }

        public override IEnumerable<string> LocalizationKeys => new[] { "Chemistry.UI" };
    }
}