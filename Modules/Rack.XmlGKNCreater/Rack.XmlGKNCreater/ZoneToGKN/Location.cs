﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Location", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
    public class Location
    {
        [XmlElement(ElementName = "OKATO", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string OKATO { get; set; }
        [XmlElement(ElementName = "KLADR", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string KLADR { get; set; }
        [XmlElement(ElementName = "OKTMO", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string OKTMO { get; set; }
        [XmlElement(ElementName = "Region", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string Region { get; set; }
        [XmlElement(ElementName = "City", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public City City { get; set; }
        [XmlElement(ElementName = "Street", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public Street Street { get; set; }
        [XmlElement(ElementName = "Level1", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public Level1 Level1 { get; set; }
    }
}