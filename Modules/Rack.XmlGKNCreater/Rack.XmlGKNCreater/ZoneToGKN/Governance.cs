﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Governance", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/sender/5.0.1")]
    public class Governance
    {
        [XmlElement(ElementName = "Name", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
        public string Name { get; set; }
        [XmlElement(ElementName = "GovernanceCode", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
        public string GovernanceCode { get; set; }
        [XmlElement(ElementName = "Location", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
        public Location Location { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
        public string Email { get; set; }
        [XmlElement(ElementName = "Telephone", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/governance/5.0.1")]
        public string Telephone { get; set; }
    }
}