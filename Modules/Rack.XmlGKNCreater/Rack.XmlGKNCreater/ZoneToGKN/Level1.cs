﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Level1", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
    public class Level1
    {
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }
    }
}