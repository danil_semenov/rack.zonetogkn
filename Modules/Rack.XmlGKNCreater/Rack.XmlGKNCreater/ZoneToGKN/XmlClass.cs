﻿using System;
using System.Text;
using System.Xml.Serialization;
using XmlApp.TerritoryToGNK;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "ZoneToGKN", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
	public class ZoneToGKN
	{
		[XmlElement(ElementName = "Title", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
		public Title Title { get; set; }
		[XmlElement(ElementName = "Declarant", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
		public Declarant Declarant { get; set; }
		[XmlElement(ElementName = "Documents", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
		public Documents Documents { get; set; }
		[XmlElement(ElementName = "NewZones", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
		public NewZones NewZones { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
		[XmlAttribute(AttributeName = "Zon5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Zon5 { get; set; }
		[XmlAttribute(AttributeName = "DocI5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string DocI5 { get; set; }
		[XmlAttribute(AttributeName = "Sen5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Sen5 { get; set; }
		[XmlAttribute(AttributeName = "Org4", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Org4 { get; set; }
		[XmlAttribute(AttributeName = "adrInp6", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string AdrInp6 { get; set; }
		[XmlAttribute(AttributeName = "Gov5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Gov5 { get; set; }
		[XmlAttribute(AttributeName = "tns", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Tns { get; set; }
		[XmlAttribute(AttributeName = "Person5", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Person5 { get; set; }
		[XmlAttribute(AttributeName = "zone4", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Zone4 { get; set; }
		[XmlAttribute(AttributeName = "NameSoftware")]
		public string NameSoftware { get; set; }
		[XmlAttribute(AttributeName = "VersionSoftware")]
		public string VersionSoftware { get; set; }
		[XmlAttribute(AttributeName = "GUID")]
		public string GUID { get; set; }
	}
}
