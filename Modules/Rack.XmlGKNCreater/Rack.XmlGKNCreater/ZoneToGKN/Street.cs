﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Street", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
    public class Street
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
    }
}