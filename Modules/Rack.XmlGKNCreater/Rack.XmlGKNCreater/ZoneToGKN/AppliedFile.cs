﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "AppliedFile", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
    public class AppliedFile
    {
        [XmlAttribute(AttributeName = "Kind")]
        public string Kind { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "GUID")]
        public string GUID { get; set; }
    }
}