﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Zone", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
    public class Zone
    {
        [XmlElement(ElementName = "CadastralDistrict", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public string CadastralDistrict { get; set; }
        [XmlElement(ElementName = "CodeZone", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public string CodeZone { get; set; }
        [XmlElement(ElementName = "CodeZoneDoc", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public string CodeZoneDoc { get; set; }
        [XmlElement(ElementName = "Locations", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public Locations Locations { get; set; }
        [XmlElement(ElementName = "SpecialZone", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
        public SpecialZone SpecialZone { get; set; }
    }
}