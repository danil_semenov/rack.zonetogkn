﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Title", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
    public class Title
    {
        [XmlElement(ElementName = "CodeDocument", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string CodeDocument { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Number", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string Number { get; set; }
        [XmlElement(ElementName = "Date", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string Date { get; set; }
        [XmlElement(ElementName = "IssueOrgan", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string IssueOrgan { get; set; }
        [XmlElement(ElementName = "Desc", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/document-info/5.0.1")]
        public string Desc { get; set; }
    }
}