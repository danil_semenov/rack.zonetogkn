﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Location", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
    public class Location2
    {
        [XmlElement(ElementName = "OKATO", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string OKATO { get; set; }
        [XmlElement(ElementName = "KLADR", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string KLADR { get; set; }
        [XmlElement(ElementName = "OKTMO", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string OKTMO { get; set; }
        [XmlElement(ElementName = "PostalCode", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "Region", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public string Region { get; set; }
        [XmlElement(ElementName = "District", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/address-input/6.0.1")]
        public District District { get; set; }
    }
}