﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "SpecialZone", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
    public class SpecialZone
    {
        [XmlElement(ElementName = "ContentRestrictions", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public string ContentRestrictions { get; set; }
        [XmlElement(ElementName = "ProtectedObject", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public string ProtectedObject { get; set; }
        [XmlElement(ElementName = "Territory", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
        public Territory Territory { get; set; }
    }
}