﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Locations", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
    public class Locations
    {
        [XmlElement(ElementName = "Location", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/zone/4.2.2")]
        public Location2 Location2 { get; set; }
    }
}