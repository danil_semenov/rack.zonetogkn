﻿using System.Xml.Serialization;

namespace XmlApp.ZoneToGKN
{
    [XmlRoot(ElementName = "Declarant", Namespace = "urn://x-artefacts-rosreestr-ru/incoming/zone-to-gkn/5.0.8")]
    public class Declarant
    {
        [XmlElement(ElementName = "Governance", Namespace = "urn://x-artefacts-rosreestr-ru/commons/complex-types/sender/5.0.1")]
        public Governance Governance { get; set; }
    }
}