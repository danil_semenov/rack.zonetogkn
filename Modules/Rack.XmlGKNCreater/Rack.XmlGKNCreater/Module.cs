﻿using ReactiveUI;
using System;
using System.ComponentModel;
using DryIoc;
using Rack.Localization;
using Rack.Shared;
using Rack.Shared.Modularity;
using System.Reactive.Linq;
using XmlApp.ViewModel;
using XmlApp;

namespace XmlApp
{
    public sealed class Module : IModule
    {
        public const string Name = "Rack.ZoneToGKN";
        private readonly DryIoc.IContainer _container;
        private readonly ApplicationTabs _applicationMenuCommands;
        private readonly IScreen _screen;

        public Module(
            DryIoc.IContainer container,
            ApplicationTabs applicationMenuCommands,
            ILocalizationService localizationService,
            IScreen screen)
        {
            _container = container;
            _applicationMenuCommands = applicationMenuCommands;
            _screen = screen;
        }

        string IModule.Name => Name;

        public void RegisterTypes()
        {
            _container.Register<IViewFor<ZoneToGKNViewModel>, ZoneToGKNMainWindow>(
                Reuse.Transient);
            // _container.RegisterInstance(new ChemistryModuleInfo(Name));
        }

        public void OnInitialized()
        {
            _applicationMenuCommands.Add(CreateMenuTab());
        }

        private ReactiveMenuTab CreateMenuTab()
        {
            return new ReactiveMenuTab(Observable.Repeat("Rack.ZoneToGKN"), new[]
            {
                new ReactiveMenuTab(Observable.Repeat("ZoneToGKN"),
                    ReactiveCommand.Create(() =>
                    {
                        _screen.Router.NavigateAndReset.Execute(
                                _container.Resolve<ZoneToGKNViewModel>())
                            .Subscribe();
                    }))
            });
        }
    }
}